$(document).ready(function(){

	/* FORMS SUBMIT BUTTON */
	$('form').submit(function(event){
		event.preventDefault();
	});


	/* TOOLTIPS */
	$("#side-nav [title]").style_my_tooltips({
		tip_delay_time:100,
    	tip_fade_speed:100,
	});

	/* SCROLL */
	$('#side-nav a').on('click', function (e) {
		e.preventDefault();

		var href = $(this).attr("href");
		var newPosition = $(href).offset().top - 86;
		
		$('html, body').animate({
			scrollTop: newPosition
		});
	});

	/* POP UP */
	$('#sign-in').click(function(e){
		e.preventDefault();
		$('#popup-si').show();
	});

	$('#sign-up').click(function(e){
		e.preventDefault();
		$('#popup-su').show();
	});

	$('#joinnow').click(function(e){
		e.preventDefault();
		$('#popup-jn').show();
	});

	$('.popup-content').click(function(){
		$(this).hide();
	}).children().click(function( event ) {
	  event.stopImmediatePropagation();
	});


/******************************************************************
								EVENTS
******************************************************************/

$(document).ready(function(){
	

	var currentPageUp = 0;
	var currentPageDown = 0;

	/*$('#start').click(function(){
		slideUp();
		slideDown();
	});*/

		var slide= setInterval(function() {
		slideUp();
		slideDown();

	}, 3000);


	function slideUp(){
		var currentSlide = $('#event-l [data-id =' + currentPageUp + ']');
		currentSlide.animate({
			top:'-100%'
		}, 1000, 'easeInBounce', function(){
			currentSlide.removeClass('active');
			currentSlide.css('top',0);
		});

		currentPageUp++;
		if (currentPageUp > 2)
			currentPageUp = 0;
		
		var nextSlide = $('#event-l [data-id =' + currentPageUp + ']');

		nextSlide.css('top','100%');
		nextSlide.addClass('active');
		nextSlide.animate({
			top:0
		}, 1000, 'easeInBounce', function(){

		});

	}

	function slideDown(){
		var currentSlide = $('#event-r [data-id =' + currentPageDown + ']');
		currentSlide.animate({
			top:'100%'
		}, 1000, 'easeInBounce', function(){
			currentSlide.removeClass('active');
			currentSlide.css('top',0);
		});

		currentPageDown++;
		if (currentPageDown > 2)
			currentPageDown = 0;
		
		var nextSlide = $('#event-r [data-id =' + currentPageDown + ']');

		nextSlide.css('top','-100%');
		nextSlide.addClass('active');
		nextSlide.animate({
			top:0
		}, 1000, 'easeInBounce', function(){

		});

	}


});

/******************************************************************
								CIRCLE
******************************************************************/

$(document).ready(function() {
	
});

});

